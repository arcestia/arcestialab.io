---
layout: post
title: Terima Kasih Para Pahlawan!!
subtitle:   Terima kasih para pahlawan negeri yang membela indonesia sampai akhirnya kita merdeka.
date:       2016-11-10
author:     "Arcestia"
header-img: "img/post-hari-pahlawan.jpg"
catalog: true
tags:
- Indonesia
- people
- Pahlawan
- Celebration
---
# Terima Kasih Pahlawan!

Pahlawan? Apa itu pahlawan? sempat terlintas dibenak kita Terhadap definisi pahlawan itu sendiri. terkadang kita bingung saat ditanyakan apa sih pahlawan? apakah yang mereka lakukan hingga disebut pahlawan? Bagiku Pahlawan adalah orang yang sudah sangat berarti perannya didalam hidup. Seorang Ibu adalah Pahlawan bagiku, Dia selalu menyertaiku disaat aku suka maupun duka, senang ataupun sedih, dan sehat ataupun sakit. Tapi yang terpenting Pahlawan yang hebat adalah pahlawan yang tidak pernah memohon atas penghargaan atau timbal balik dari perbuataannya.

Jadi marilah kita apresiasikan pahlawan yang gugur membela tanah air kita. Bersyukurlah kita sudah terbebas dari penjajahan dan hidup merdeka.

Selamat Hari Pahlawan Teman. <br>
See ya di post gw selanjutnya.

Arcestia, <br>
October 28th, 2016
