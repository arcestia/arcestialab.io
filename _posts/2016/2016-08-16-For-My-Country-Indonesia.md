---
layout: keynote
title: For My Country Indonesia
subtitle: Indonesia is wonderful country that having great culture, and I was lucky be part of it.
iframe: "https://www.youtube.com/embed/GenukHtqZiw?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1"
date:       2016-08-16
author:     "Arcestia"
tags:
- Indonesia
- life
- people
- culture
---

Tomorrow August 17th is Indonesia Independence days, 71th Years ago our Founding father proclamate our country to Independence.
Sukarno and Mohammad Hatta are the founding fathers of Indonesia. They both signed the Proclamation of Independence which then read by Sukarno, proclaiming the independence of Indonesia from the Netherlands on 17 August 1945. A day later, they were elected respectively as the first President and Vice President of Indonesia. As the Netherlands did not recognize the independence, both of them were prominent figures and were seen as symbol of unity among Indonesian people to fight against Dutch during the National Revolution from 1945 to 1949. In August 1949, Hatta headed a delegation to the Hague for a Round Table Conference which then led to the recognition of Indonesian independence by the Netherlands on 23 December 1949.

For All you guys happy independence days, check this video below

<iframe width="560" height="315" src="https://www.youtube.com/embed/lUkYLgOhrpo?rel=0" frameborder="0" allowfullscreen></iframe>
