---
layout: post
title: Selamat Hari Sumpah Pemuda!!
subtitle:   Bersatulah, para pemuda untuk indonesia yang lebih baik.
date:       2016-10-28
author:     "Arcestia"
header-img: "img/post-bg-sumpahpemuda.jpg"
catalog: true
tags:
- Indonesia
- people
- Sumpah
- Pemuda
- Celebration
---
# Selamat Hari Sumpah Pemuda!

Pertama-pertama saya sebagai Pemuda Indonesia mengajak para Pemuda lainnya, Ayo mari kita bergerak peduli akan negeri kita sendiri. Sesuai dengan apa yang kita ikrarkan untuk menjadi indonesia yang satu tanpa adanya perbedaan. Perbedaan adalah kekuatan kita bukan melainkan menjadi kelemahan. Semangat perbedaan yang justru membuat Indonesia semakin kaya akan budaya. Gebyarkan Semangat "**Bhinneka Tunggal Ika**" teman-teman pemuda sekalian.

# 1 Tahun ngeblog dengan static site.

Banyak suka dan duka jika buat blog dengan static site. Hal pertama adalah berbedanya format penulisannya. Jika di Wordpress, Blogger atau lainnya mereka ada Text Editor sendiri. Tapi di static site kita musti pake Markdown. Bagi orang awam yang baru pindah Markdown adalah hal yang bikin mereka balik kembali dengan CMS. Tapi saya pecinta akan perbedaan oleh karena itu saya melanjutkan dengan static site hingga sekarang.

# Diskriminasi Gender

Ini adalah salah satu isu perbedaan yang sering kali dibahas didunia. Logikanya adalah bahwa sebenarnya itu Hak seseorang untuk memilih hidupnya sendiri. Apa salahnya LGBT? Apa urusan kita dengan mereka. lagipula itu hidup mereka bukan hidup kita, jadi apa yang musti dipikirin. Mereka Ganggu? Mereka gak akan ganggu kalau kita gak memulai duluankan. Jadi ayolah Perbedaan adalah suatu hal yang indah dan jangan lari dari perbedaan.

# Yuk, Mari kita Cintai Perbedaan

Pada akhirnya harus gw akui bahwa banyak orang gak bisa menerima Perbedaan, tapi gimana mau bisa menikmati Perbedaan kalau kita sendiri susah menolaknya. Yuk kita terima bersama Perbedaan dengan semangat **Bhinneka Tunggal Ika** dan semangat Para Pemuda yang dahulu bersatu untuk Indonesia.

Selamat Hari Sumpah Pemuda kawan. <br>
See ya di post gw selanjutnya.

Arcestia, <br>
October 28th, 2016
